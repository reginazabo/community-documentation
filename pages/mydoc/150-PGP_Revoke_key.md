---
title: PGP - Revoking old key from key servers
keywords: email, PGP, GPGTools, GPG Suite, Mac, Enigmail, Thunderbird, revocation, key management, email security
last_updated: November 6, 2018
tags: [secure_communications, articles]
summary: "A key needs to be revoked because it has been compromised or the client has lost their private key or has generated a new key."
sidebar: mydoc_sidebar
permalink: 150-PGP_Revoke_key.html
folder: mydoc
conf: Public
lang: en
---


# PGP - Revoking old key from key servers
## How to revoke a user ID or key from key servers using GPGTools for Mac or Enigmail

### Problem


A key needs to be revoked because it has been compromised or the client has lost their private key or has generated a new key. The client is using Enigmail or GPGTools on Mac.

- The client doesn't know how to revoke their key.
- The client has lost their revocation certificate.


* * *


### Solution

It is always a good practice to revoke unused keys from the key servers. Ignoring this may result in confusion, decryption issues, and security/privacy breaches.
 
An old PGP key may no longer be used because:

- a new PGP key has been created
- access to the secret key has been lost
- the passphrase for the secret key has been forgotten
- a user ID is associated with a new PGP key

Note that revoking a key will require either of the following:

1. access to the secret key and passphrase
2. a revocation certificate

If neither of the above is available, a revocation will not be possible. In that case, the owner of the key should notify her/his contacts that they should stop using (or delete) the old public key and use the new public key when encrypting email or files addressed at the client.


#### Enigmail

Instructions on how to revoke a PGP key with Enigmail can be found [here](https://www.enigmail.net/documentation/Key_Management#Revoking_your_key_pair).


#### GPGTools

A guide for revoking a user ID or key from the key servers using GPGtools for Mac can be found [here](http://support.gpgtools.org/kb/gpg-keychain-faq/how-to-revoke-a-key-or-userid-and-can-i-delete-a-key-from-the-key-servers).


* * *


### Comments



* * *


### Related Articles
