---
title: Set up Bitmask VPN client with Riseup Black account
keywords: circumvention, VPN, censorship, Bitmask, Riseup, Linux, Android
last_updated: July 19, 2018
tags: [anonymity_circumvention, articles]
summary: "Client is connecting through a monitored connection; Connection to Tor is blocked; Client's Internet connection is censored; Client requires Internet access through open or insecure connections"
sidebar: mydoc_sidebar
permalink: 192-Riseup-Bitmask-VPN.html
folder: mydoc
conf: Public
lang: en
---


# Set up Bitmask VPN client with Riseup Black account
## How to assist a client (Linux or Android user) who needs a free VPN account

### Problem

The client needs a secure connection to the Internet.

### Solution

This article is focused in the configuration of Riseup Bitmask VPN. If you require a more comprehensive view on anonymity/censorship tools, or are looking for additional information on VPNs, check [Article #175: FAQ - Circumvention &amp; Anonymity tools](175-Circumvention_Anonymity_tools_list.html).

If you already decided Riseup Bitmask VPN is an appropriate solutions for the client, you can follow these steps:

- Provide the client with the Riseup links:
    - [Linux](https://help.riseup.net/en/vpn/how-to/linux)
    - [Android](https://help.riseup.net/en/vpn/how-to/android)

- If the client is not technical enough to follow the steps, set up a meeting time to assist.


### Comments

If the client needs VPN on a machine/device other than Andorid or Linux, we can consider to provide the client with Access Now VPN. (see Article #82).

Please keep in mind that this solution is for **exceptional** cases, when neither Tor nor pluggable transports work, and it can be provided only for temporary periods of time.


* * *

### Related Articles

- [Article #175: FAQ - Circumvention &amp; Anonymity tools](175-Circumvention_Anonymity_tools_list.html)
