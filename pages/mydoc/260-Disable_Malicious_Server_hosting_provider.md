---
title: Disable C&amp;C Server - Email to Hosting Provider
keywords: C&amp;C server, malware, email templates, hosting provider, malicious website
last_updated: July 20, 2018
tags: [vulnerabilities_malware_templates, templates]
summary: "Template to ask the company that hosts a malicious website to disable it"
sidebar: mydoc_sidebar
permalink: 260-Disable_Malicious_Server_hosting_provider.html
folder: mydoc
conf: Public
lang: en
---


# Disable C&amp;C Server - Email to Hosting Provider
## Template for writing to the company that hosts a malicious website to disable it

### Body

Dear [Name],

I am [IH's name] from Access Now's Digital Security Helpline team - https://www.accessnow.org/help. We have received reports that a website hosted within your premises is infecting visitors with malware:

- [URL] *[replace http with hxxp to prevent infections]*
- [IP]

In the above URL(s), http has been replaced with hxxp to prevent
accidental infections.

*[Replace the following paragraph with your analysis of the situation]*

There is a malicious JavaScript injected into several web pages. The
script loads 3rd-party content onto the PC of the visitor from the following URL:

- [URL]

Please remove all malicious content from your web server. Patch all
applications (content management systems, plugins, etc.) and server
components (web server, FTP, etc.) to the latest version and review
the security settings of all components.

You can find a collection of best practices on this web site:

- https://www.circl.lu/pub/tr-26/

Please, don't hesitate to get back to us if you have further questions or need
additional support.

We would appreciate it if you could confirm the receipt of this report.

With best regards,

[IH's name]


* * *


### Related Articles

- [Article #219: Targeted Malware: Disable Malicious C&amp;C Server](219-Targeted_Malware_Disable_Malicious_Server.html)
- [Article #259: Disable C&amp;C server - email to registrar of malicious domain](259-Disable_Malicious_Server_registrar.html)
