---
title: Initial Reply - For Russian Speakers
keywords: email templates, initial reply, case handling policy
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Russian speakers"
sidebar: mydoc_sidebar
permalink: 265-Initial_Reply_For_Russian_Speakers.html
folder: mydoc
conf: Public
lang: ru
---


# Initial Reply
## First response, Email to Client for Russian speakers

### Body

Уважаемая(ый) $ClientName,

Спасибо за обращение в Службу поддержки по цифровой безопасности Access
Now (https://www.accessnow.org/help). Меня зовут $IHName.

Мы получили ваш запрос, и в настоящее время моя команда его обрабатывает.

Пожалуйста учтите, что наша Служба поддержки организована группой
специалистов по безопасности, находящихся в разных временных зонах.
Поэтому вам может ответить другой участник нашей команды. Это зависит от
того, в какое время и в какой день недели мы получили ваше сообщение. В
ближайшее время мы свяжемся с вами для обсуждения деталей вашего запроса.

Пожалуйста, оставляйте “[accessnow #ID]” в поле темы письма для всех
сообщений, касающихся обсуждения этого запроса. Это идентификационный
номер запроса, сгенерированный нашей системой обеспечения поддержки для
координации всех задач, связанных с обработкой вашего запроса.

С уважением,

$IHName


* * *


### Related Articles

- [Article #17: Initial Reply in English](17-Initial_Reply.html)
- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
