---
title: Recommendations on Two-Factor Authentication
keywords: authentication, verification, 2-factor authentication, two-factor authentication, 2-step verification, 2FA, multi-factor authentication, authentication security
last_updated: October 26, 2018
tags: [account_security, articles, faq]
summary: "A client needs advice on how to secure their account with multi-factor authentication."
sidebar: mydoc_sidebar
permalink: 294-FAQ-2FA.html
folder: mydoc
conf: Public
lang: en
---


# Recommendations on Two-Factor Authentication
## Advice on best solutions for two-factor authentication

### Problem

A client needs to secure their account against hijacking and hasn't enabled up 2-password authentication yet.

A high-risk client has set up SMS-based 2-factor authentication and we need to advise them to use a more reliable solution.


* * *


### Solution

Two-factor authentication (2FA) offers greater account security by requiring the user to authenticate their identity with more than one method. This means that, even if someone were to get hold of their target's primary password, they could not access their account unless they also had their mobile phone or another secondary means of authentication.

We should warn the client that when enabling 2-factor authentication they should always prefer an alternative method to SMS-based 2FA, as this is more vulnerable to hijacking. For more information, we can link [this article](https://www.accessnow.org/need-talk-sms-based-two-step-authentication/).

#### General Resources

- [EFF's guide on how to enable two-factor authentication](https://ssd.eff.org/en/module/how-enable-two-factor-authentication)

- [Access Now's infographic to illustrate 2FA and its methods](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png).

#### Instructions for online services

What follows is a list of links with instructions on how to enable 2-factor authentication in the most popular online services:

- [Google](https://www.google.com/landing/2step/)
    - If the client is at high risk of account hijacking and does not need to download their email or to encrypt it with Thunderbird+Enigmail or another mail client, we can suggest them to enable the [Google Advanced Protection Program](https://landing.google.com/advancedprotection/) and guide them through the steps.
    - Also see [Article #90: Two-Factor Authentication for Google Account](90-Google_2FA.html)

- [Facebook](https://www.facebook.com/help/148233965247823)

- [Twitter](https://help.twitter.com/en/managing-your-account/two-factor-authentication) (only SMS-based)

- [Tumblr](https://tumblr.zendesk.com/hc/en-us/articles/226270148)

- [Instagram](https://help.instagram.com/566810106808145)

- Microsoft:
    - [Microsoft account](http://windows.microsoft.com/en-us/windows/two-step-verification-faq)
    - [Outlook, msn.com, hotmail.com](https://www.eff.org/de/deeplinks/2016/12/how-enable-two-factor-authentication-outlookcom-and-microsoft)
    - [Outlook and Hotmail](https://www.msoutlook.info/question/773)

- [Yahoo](https://help.yahoo.com/kb/SLN5013.html)
    - Also see [Article #168: Secure Yahoo Account with 2-Step Verification](168-Yahoo_2FA.html)

- [iCloud](https://support.apple.com/en-us/HT204915)

- [Protonmail](https://protonmail.com/support/knowledge-base/two-factor-authentication/)

- [Tutanota](https://tutanota.uservoice.com/knowledgebase/articles/1201942-how-does-two-factor-authentication-2fa-work-in-t)
 
- [xs4all](https://www.xs4all.nl/service/diensten/email/installeren/geavanceerd/inloggen-met-otp-calculator.htm) (only in Dutch)

- [Mailbox.org](https://userforum-en.mailbox.org/knowledge-base/article/is-there-a-two-factor-authentication)

- [Autistici.org](https://www.autistici.org/docs/2FA)


* * *


### Comments

If a client's mail server is gmx.de, we can link these [instructions in German for securing account access without 2FA](https://www.henning-uhle.eu/informatik/was-wurde-aus-der-zwei-faktor-authentifizierung-bei-gmx).



* * *


### Related Articles

- [Article #90: Two-Factor Authentication for Google Account](90-Google_2FA.html)
- [Article #168: Secure Yahoo Account with 2-Step Verification](168-Yahoo_2FA.html)
