---
title: Android App Removal
keywords: Shutdown, Android App, App Store, Play Store
last_updated: November 02, 2018
tags: [censorship, articles]
summary: "An app created by a civil society member has disappeared from the Android app store."
sidebar: mydoc_sidebar
permalink: 296-Android_App_Removal.html
folder: mydoc
conf: Public
lang: en
---


# Android App Removal
## How to help a HRD or NGO who has had their Android app removed from the Play Store 

### Problem

Some human rights defenders, NGOs and media outlets develop and publish Android apps to share their content, articles, or campaigns. However these apps could be targeted by adversaries, who may report any content violating the Google Play Terms of Service and request Google to take down the app.

The client usually finds out about the takedown of their app when a supporter notifies them or upon a random check, as according to their policy Google is not bound to send a warning before taking an app down.

Once the app is removed, the owner, who could be a human rights defender, NGO or media outlet, loses an important tool in their work to reach out to their audience and sympathizers. Sometimes the app is taken down when the client's website has already been censored. In such cases, the app is their last instance solution, and its removal constitutes a big loss.


* * *


### Solution

The most immediate solution to a takedown of an app is for the developer to re-upload a compliant version that does not include the violating content.

The incident handler should help the beneficiary identify the violating content and remove it, as in most cases removing the content is better then losing the app on the Play Store. 

The content could have been reported as:

- Copyrighted content  
- Trademark infringement
- [Inappropriate Content](https://play.google.com/about/restricted-content/inappropriate-content/)

Once the content is identified, the beneficiary should work with the developer to remove this content and re-upload it. 

If the app has been removed by mistake, an [appeal](https://support.google.com/googleplay/android-developer/answer/2477981?hl=en) can be filed.

If the beneficiary needs to keep all content in their app and cannot remove anything, we can recommend the app to be uploaded to [F-Droid](https://f-droid.org) or to post the link to the .apk file on Github or Gitlab on social media platforms. 


* * *


### Comments
* * *


### Related Articles
